package com.nikasov.tasklist;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class App extends Application {

    public static final String ID = "id";
    public static final String LOG_TAG = "DEB_TAG";
    public static final String NEW_TASK = "new_task";

    public static RealmConfiguration realmConfig;

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(getApplicationContext());

        realmConfig = new RealmConfiguration.Builder()
                .name("myrealm.realm")
                .schemaVersion(1)
                .deleteRealmIfMigrationNeeded()
                .build();
    }
}
