package com.nikasov.tasklist.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nikasov.tasklist.App;
import com.nikasov.tasklist.R;
import com.nikasov.tasklist.activity.CardActivity;
import com.nikasov.tasklist.db.TaskItem;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

import static com.nikasov.tasklist.App.LOG_TAG;

public class TasksAdapter
        extends RecyclerView.Adapter<TasksAdapter.ViewHolder>
        implements RealmChangeListener {

    private Context context;
    private RealmResults<TaskItem> taskItems;

    public TasksAdapter(Context context, RealmResults<TaskItem> taskItems) {
        this.context = context;
        this.taskItems = taskItems;

        taskItems.addChangeListener(this);
    }

    @NonNull
    @Override
    public TasksAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_task, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull TasksAdapter.ViewHolder holder, int position) {
        TaskItem taskItem = taskItems.get(position);

        holder.date.setText(getStringDate(taskItem.getDate()));
        holder.title.setText(taskItem.getCaption());

        holder.item.setOnClickListener(v -> {
            Log.d(LOG_TAG, "Selected task id: " + taskItem.getId());

            Intent intent = new Intent(context, CardActivity.class);
            intent.putExtra(App.ID, taskItem.getId());
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return taskItems.size();
    }

    private String getStringDate(Date date){

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd", Locale.getDefault());

        return dateFormat.format(date);
    }

    @Override
    public void onChange(Object o) {
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.item)
        FrameLayout item;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
