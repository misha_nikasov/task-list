package com.nikasov.tasklist.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.nikasov.tasklist.App;
import com.nikasov.tasklist.R;
import com.nikasov.tasklist.adapter.TasksAdapter;
import com.nikasov.tasklist.db.TaskItem;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

import static com.nikasov.tasklist.App.LOG_TAG;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.recycler)
    RecyclerView recyclerView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        init();
        setToolbar();
        setList();
    }

    private void init() {
        realm = Realm.getInstance(App.realmConfig);
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.toolbar_title));
    }

    private void setList() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        TasksAdapter adapter = new TasksAdapter(this, getTasksList());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.setNestedScrollingEnabled(true);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(
                recyclerView.getContext(),
                layoutManager.getOrientation());

        recyclerView.addItemDecoration(dividerItemDecoration);
    }

    private RealmResults<TaskItem> getTasksList(){
        return realm.where(TaskItem.class).findAll();
    }

    private void addTask(String caption, Date date, String status){
        realm.executeTransaction(realm -> {
            TaskItem profile = realm.createObject(TaskItem.class, UUID.randomUUID().toString());
            profile.setCaption(caption);
            profile.setDate(date);
            profile.setStatus(status);

            Intent intent = new Intent(this, CardActivity.class);
            intent.putExtra(App.ID, profile.getId());
            this.startActivity(intent);

            Log.d(LOG_TAG, "New task created");
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.action_search) {
            addTask("New Task", Calendar.getInstance().getTime(), "Scheduled");
        }
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }
}
