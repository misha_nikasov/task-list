package com.nikasov.tasklist.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.nikasov.tasklist.App;
import com.nikasov.tasklist.R;
import com.nikasov.tasklist.db.TaskItem;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;
import io.realm.Realm;

import static com.nikasov.tasklist.App.LOG_TAG;

public class CardActivity extends AppCompatActivity {

    @BindView(R.id.title)
    TextView titleTextView;
    @BindView(R.id.date)
    TextView dateTextView;
    @BindView(R.id.status)
    TextView statusTextView;

    private Realm realm;
    private TaskItem taskItem;

    private String title, status;
    private Date date;

    private String currentTaskId;

    private DatePickerDialog.OnDateSetListener dateSetListener;
    private final Calendar myCalendar = Calendar.getInstance();

    private String[] statusType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card);

        ButterKnife.bind(this);

        init();
    }

    @OnClick(R.id.close_btn)
    void closeCard(){
        finish();
    }

    @OnLongClick(R.id.status)
    void changeStatus(){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.choose));

        builder.setSingleChoiceItems(statusType, checkPos(status), (dialog, which) -> {
            switch (which) {
                case 0:
                    status = statusType[0];
                    dialog.dismiss();
                    break;
                case 1:
                    status = statusType[1];
                    dialog.dismiss();
                    break;
                case 2:
                    status = statusType[2];
                    dialog.dismiss();
                    break;
            }
            updateView();
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private int checkPos(String type) {
        for (int i = 0; i < statusType.length ; i++){
            if (statusType[i].equals(type)){
                return i;
            }
        }
        return 0;
    }

    @OnLongClick(R.id.date)
    void changeDate() {
        new DatePickerDialog(this, dateSetListener,
                myCalendar.get(Calendar.YEAR),
                myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    @OnLongClick(R.id.title)
    void changeTitle() {

        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_caption_edit, null);
        EditText editText = dialogView.findViewById(R.id.edit_caption);

        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle(getResources().getString(R.string.edit_caption))
                .setView(dialogView)
                .setPositiveButton(getResources().getString(R.string.save), (dialog1, which) -> {
                    title = String.valueOf(editText.getText());
                    updateView();
                })
                .setNegativeButton(getResources().getString(R.string.cancel), null)
                .create();


        dialog.show();
    }

    private void getTask() {
        realm.executeTransaction(bgRealm -> {
            taskItem = realm.where(TaskItem.class).equalTo("id", currentTaskId).findFirst();
            Log.d(LOG_TAG, "Current task: " + taskItem.getCaption());
            title = taskItem.getCaption();
            date = taskItem.getDate();
            status = taskItem.getStatus();

            myCalendar.setTime(date);

            updateView();
            setDate();
        });
    }

    private void init() {

        realm = Realm.getInstance(App.realmConfig);

        statusType = new String[]{
                getResources().getString(R.string.scheduled),
                getResources().getString(R.string.due),
                getResources().getString(R.string.dismissed)
        };

        currentTaskId = getIntent().getStringExtra(App.ID);
        getTask();
    }

    private void updateView() {
        titleTextView.setText(title);
        dateTextView.setText(getStringDate(date));
        statusTextView.setText(status);
    }

    private String getStringDate(Date date){
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd", Locale.getDefault());
        return dateFormat.format(date);
    }

    private void setDate(){
        dateSetListener = (view, year, monthOfYear, dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            date = myCalendar.getTime();
            updateView();
        };
    }

    private void saveCard() {
        realm.executeTransaction(realm -> {
            TaskItem profile = realm.where(TaskItem.class).equalTo("id" ,currentTaskId).findFirst();
            profile.setCaption(title);
            profile.setDate(date);
            profile.setStatus(status);

            Log.d(LOG_TAG, taskItem.getCaption() + " saved");
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        saveCard();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }
}
